<!DOCTYPE html>
<html lang="de">
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
   
        <script src="Scripts/jquery-2.1.3.js"></script>
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet" />
        <link href="bootstrap/css/signin.css" rel="stylesheet">
        <script src="bootstrap/js/bootstrap.js"></script>


        <title>Anmeldung Kassenprogramm</title>
        
   
        
        <script src="sha1.js"></script>
        <script>
        $(document).ready(function(){
            
            $("#ok").click(function(){
                var pass = $("#passwort").val();
                var hash = CryptoJS.SHA1(pass);
                hash = hash.toString();
                var user = $("#benutzername").val();
                $.ajax({
                        type: "POST",
                        url: "login.php",
                        data: {benutzername : user,passwort : hash},
                        dataType: "text",
                        success: function(data)
                        {
                            if(data == "Erfolg")
                            {
                                window.location.replace("buchungen.php");
                            }
                            else 
                            {
                                alert(data);
                            }
                        }
                });
                return false;
            });
        });
        </script>
    </head>

    <body>
        <div class="container">
            <form id="loginform" class="form-signin">
                <h2 class="form-signin-heading">Bitte anmelden</h2>
                <input name="benutzername" type="text"  class="form-control" placeholder="Benutzername" id="benutzername">
                <input name="passwort" type="password" class="form-control" placeholder="Passwort" id="passwort">
                <button id="ok" class="btn btn-lg btn-primary btn-block" >Bestätigen</button>
            </form>    
        </div>
    </body>
</html>