<div class="col-lg-2">
    <form class="form" role="form" id="umbuchung_form">
        <div class="form-group" id="datum_group">
            <label for="datum">Datum</label>
            <input type="date" name="datum" class="form-control " id="datum" style="height: 34px">
        </div>
        <div class="form-group" id="betrag_group">
            <label for="betrag" >Betrag</label>
            <input type="number" name="betrag" class="form-control" id="betrag" style="height: 34px">
        </div>
        <div class="form-group">
            <label for="verwendungszweck">Verwendungszweck</label>
            <input type="textarea" name="verwendungszweck" class="form-control" id="verwendungszweck">
        </div>
        <div class="form-group" id="quelle_group">
            <label for="quelle">Quelle</label>
            <select name="quelle" class="form-control" id="quelle">
                <?php
                $dropdown = "";

                $array = explode(";", $values_konto);

                foreach ($array as $value) {
                    $array2 = explode(":", $value);
                    $dropdown = $dropdown . "<option value='$array2[0]'>$array2[1]</option>";
                }
                echo $dropdown;
                ?>
            </select>
        </div>
        <div class="form-group" id="ziel_group">
            <label for="ziel">Ziel</label>
            <select name="ziel" class="form-control" id="ziel">
                <?php
                echo $dropdown;
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="buchungsgruppe">Buchungsgruppe</label>
            <select name="buchungsgruppe" class="form-control" id="buchungsgruppe">
                <?php
                $dropdown = "";

                $array = explode(";", $values_BG);

                foreach ($array as $value) {
                    $array2 = explode(":", $value);
                    $dropdown = $dropdown . "<option value='$array2[0]'>$array2[1]</option>";
                }
                echo $dropdown;
                ?>
            </select>
        </div>
        <button type="submit" class="btn btn-default" id="umbuchung_submit">Bestätigen</button>
    </form>
</div>

