<?php

// Enthält Definitionen für DB-Parameter(PHPGRID_DBHOST,PHPGRID_DBUSER,...)
include_once("config.php");
// Lädt Spaltenbreiten aus DB
include("lade_spaltenbreiten.php");
// Enthält alle Bibliotheken des Frameworks
include(PHPGRID_LIBPATH . "inc/jqgrid_dist.php");


// Verbindung zur DB aufbauen
mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS);
mysql_select_db(PHPGRID_DBNAME);


// Erstelle neues Grid-Objekt
$buchungen = new jqgrid();

// Setzt einige Parameter zur Darstellung
$optionen["caption"] = "Buchungen"; // Überschrift
$optionen["sortname"] = 'Datum';    // Zu Sortierende Spalte
$optionen["sortorder"] = "desc";    // Setzt Sortierrichtung
$optionen["autowidth"] = true;      // Passt Breite des Grid an Bildschirmbreite an
$optionen["forceFit"] = true;       // Passt Spaltenbreiten an Gridbreite an
$optionen["height"] = "";           // Passt Höhe an Anzahl der Zeilen angezeigten Zeilen an
$optionen["add_options"] = array('width' => '383'); //Setzt Breite des "Neue Zeile"-Dialogs
$optionen["add_options"]["afterShowForm"] = "function(){defaults();}";
$optionen["edit_options"] = array('width' => '383'); //Setzt Breite des "Bearbeite Zeile"-Dialogs
// Setzt einige Parameter zum Export
$optionen["export"] = array("filename" => "Buchungen", // Setzt Dateiname
    "heading" => "Buchungen", // Setzt Überschrift
    "orientation" => "landscape", // Setzt Orientierung(Hochformat/Querformat)
    "paper" => "a4");                       // Setzt Papiergröße
$optionen["export"]["sheetname"] = "Buchungen";         // Setzt Name der Tabelle bei XLS-Export
$optionen["export"]["range"] = "filtered";              // Beschränkt Export auf gefilterte Zeilen
$optionen["export"]["render_type"] = "html";



// Füge alle oben gesetzten Parameter dem Grid-Objekt hinzu.
$buchungen->set_options($optionen);

$buchungen->set_actions(array(
    "add" => true, // Erlaube hinzufügen von Zeilen
    "edit" => true, // Erlaube berbeiten von Zeilen
    "delete" => true, // Erlaube löschen von Zeilen
    "rowactions" => true, // Erlaube Zeilenoperationen 
    "export_excel" => true, // Aktiviere Excel-Export
    "export_pdf" => true, // Aktiviere PDF-Export
    "autofilter" => true, // Aktiviere Schnellfilter 
    "search" => "advance"   // Aktiviere fortgeschrittenen Filter
        )
);


// Bestimme DB-Tabelle für Update, Insert, Delete
$buchungen->table = "buchungen";

// Bestimme SQL-Abfage zum laden der Daten
$buchungen->select_command = "SELECT buchungen.ID,Datum, Betrag, Verwendungszweck,Empfaenger, Art,
                    konten.IBAN as Konto_ID, buchungsgruppen.Name as BGruppe_ID
                    FROM buchungen  JOIN buchungsgruppen ON buchungen.BGruppe_ID = buchungsgruppen.ID
                                    JOIN konten ON buchungen.Konto_ID = konten.ID
                    WHERE YEAR(buchungen.Datum) = " . $_SESSION["jahr"];

// <editor-fold defaultstate="collapsed" desc="Spaltenforamtierung">
// Bestimme die Formatierung alle Spalten

$spalte = array();             // Erstelle neues Array zum speichern der Parameter
$spalte["title"] = "ID";       // Bestimme Überschrift
$spalte["name"] = "ID";        // Bestimme Name
$spalte["width"] = $spaltenbreiten["buchungen"]["ID"][$_SESSION["benutzername"]]; // Bestimme Breite(wird von lade_spaltenbreiten.php bereitgestellt)
$spalte["editable"] = false;   // Bestimmt ob  Benutzter Spalte bearbeitet darf
$spalte["hidden"] = true;      // Bestimmt ob Spalte angezeigt wird
$spalte["export"] = false;     // Bestimmt ob Spalte exportiert wird
$spalten[] = $spalte;             //Speichere Spaltenparameter in Array


$spalte = array();
$spalte["title"] = "Datum";
$spalte["name"] = "Datum";
$spalte["width"] = $spaltenbreiten["buchungen"]["Datum"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["formatter"] = "date"; // Spalte wird als Datum formatiert 
$spalte["formatoptions"] = array("srcformat" => 'Y-m-d', "newformat" => 'd.m.Y'); // Bestimme Ausgangs- und Zielformat
$spalte["editrules"] = array("required" => true); // Bestimme ob Feld leer sein darf
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Betrag";
$spalte["name"] = "Betrag";
$spalte["width"] = $spaltenbreiten["buchungen"]["Betrag"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["formatter"] = "number"; // Spalte wird als Zahl formatiert
$spalte["formatoptions"] = array("thousandsSeparator" => ".", // Bestimme Dezimal- und Tausendertrennzeichen und
    "decimalSeparator" => ",", // Dezimalstellen
    "decimalPlaces" => 2);
$spalte["editrules"] = array("required" => true);
$spalte["editrules"] = array("number" => true);      // Legt fest dass, beim einfügen einer neuen Zeile eine Zahl eingegeben werden muss
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Verwendungszweck";
$spalte["name"] = "Verwendungszweck";
$spalte["width"] = $spaltenbreiten["buchungen"]["Verwendungszweck"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["edittype"] = "textarea"; // Formatiere Spalte als Textfeld
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Empfänger / Einzahler";
$spalte["name"] = "Empfaenger";
$spalte["width"] = $spaltenbreiten["buchungen"]["Empfaenger"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["formatter"] = "autocomplete";     // Stellt Autovervollständigung zur Verfügung
$spalte["formatoptions"] = array("sql" => "SELECT distinct Empfaenger as k, Empfaenger as v from buchungen",
    "search_on" => "Empfaenger", // Bestimmt in welcher DB-Spalte gesucht wird
    "update_field" => "Empfaenger");    // Bestimmt welche DB-Spalte aktualisiert wird
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Art";
$spalte["name"] = "Art";
$spalte["width"] = $spaltenbreiten["buchungen"]["Art"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["edittype"] = "select"; // Formatiert Spalte als Dropdownmenü
$spalte["editoptions"] = array("value" => 'E:E;A:A;K:K', "defaultValue"=>$defaults["Art"]); // Bestimmt Auswahloptionen
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Konto";
$spalte["name"] = "Konto_ID";
$spalte["width"] = $spaltenbreiten["buchungen"]["Konto_ID"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["edittype"] = "select";
$values_konto = $buchungen->get_dropdown_values("select distinct ID as k, IBAN as v from konten"); // Lädt Auswahloptionen aus DB
$spalte["editoptions"] = array("value" => $values_konto,"defaultValue"=>$defaults["Konto_ID"]);
$spalte["stype"] = "select";       // Formatiert Suchfeld als Dropdownmenü
$spalte["searchoptions"] = array("value" => ":;" . $values_konto); // Benutzt gleiche Auswahloptionen wie oben
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Buchungsgruppe";
$spalte["name"] = "BGruppe_ID";
$spalte["width"] = $spaltenbreiten["buchungen"]["BGruppe_ID"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["edittype"] = "select";
$values_BG = $buchungen->get_dropdown_values("select ID as k,Name as v from buchungsgruppen");
$spalte["editoptions"] = array("value" => $values_BG,"defaultValue"=>$defaults["BGruppe_ID"]);
$spalte["stype"] = "select";
$spalte["searchoptions"] = array("value" => ":;" . $values_BG);
$spalten[] = $spalte;


$buchungen->set_columns($spalten, false); // Füge Parameter zum Grid-Objekt hinzu
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Zeilenformatierung">
// Bestimme Zeilenformatierung
// Formatierung für Spalte Betrag
// Wenn Betrag größer als Null setzte Farbe auf Grün
$zeile = array();
$zeile["column"] = "Betrag"; // Setzt Spaltenname
$zeile["op"] = ">";         // Setzt Vergleichsoperator
$zeile["value"] = "0";      // Setzt Vergleichswert
$zeile["cellcss"] = "'color':'green'"; // Setzt Formatierungsoptionen mit CSS
$zeilen[] = $zeile;
// Wenn Betrag kleiner als Null setzte Farbe auf Rot
$zeile = array();
$zeile["column"] = "Betrag";
$zeile["op"] = "<";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'red'";
$zeilen[] = $zeile;

$buchungen->set_conditional_css($zeilen); // Füge Parameter zum Grid-Objekt hinzu
//</editor-fold>
// Bestimmte eigene Methode zum hinzufügen einer Zeile
function add_row($data)
{

    // Wenn Spalte "Art" den Wert "A", und Spalte "Betrag" größer als Null ist,
    // verändere Vorzeichen von Betrag
    if ($data["params"]["Art"] == "A" && $data["params"]["Betrag"] > 0)
    {

        $data["params"]["Betrag"] = $data["params"]["Betrag"] * (-1);
    }
    // Wenn Spalte "Art" den Wert "E", und Spalte "Betrag" kleiner als Null ist,
    // verändere Vorzeichen von Betrag
    else if ($data["params"]["Art"] == "E" && $data["params"]["Betrag"] < 0)
    {

        $data["params"]["Betrag"] = $data["params"]["Betrag"] * (-1);
    }
}

function set_pdf_format($param)
{
    $grid = $param["grid"];
    $arr = $param["data"];
    
    $breite["Datum"] = "9";
    $breite["Betrag"] = "9";
    $breite["Verwendungszweck"] = "33";
    $breite["Empfaenger"] = "15";
    $breite["Art"] = "4";
    $breite["Konto_ID"] = "7";
    $breite["BGruppe_ID"] = "23";
    
    
    
    $html = "";
    $html .= "<h1>" . $grid->options["export"]["heading"] . "</h1>";
    $html .= '<table border="0" cellpadding="4" cellspacing="2">';
    
    
    
    foreach ($arr as $v)
    {
        $shade = ($i++ % 2) ? 'bgcolor="#efefef"' : '';
        $html .= "<tr>";
        foreach ($v as $k => $d)
        {
            
            if ($i == 1)
                $html .= "<td bgcolor=\"lightgrey\" style = \" width:$breite[$k]%\"><strong>$d</strong></td>";
            else if($k == "Betrag" && $d >= 0)
            {
                $html .= "<td $shade style = \" color:green; width:$breite[$k]%\">$d</td>";
            }
            else if($k == "Betrag" && $d <= 0)
            {
                $html .= "<td $shade style = \" color:red; width:$breite[$k]%\">$d</td>";
            }
            else
            {
                $html .= "<td $shade style = \" width:$breite[$k]%\">$d</td>";
            }
        }
        $html .= "</tr>";
    }

    $html .= "</table>";
    return $html;
}

//Setzte Parameter für PHPGrid-Events
$e["on_render_pdf"] = array("set_pdf_format", null);
$e["on_insert"] = array("add_row", null, true); // Bestimmt die aufzurufende Funktion
$e["on_update"] = array("add_row", null, true);
$buchungen->set_events($e);     // Füge Parameter zum Grid-Objekt hinzu
// Generiere HTML, CSS, JS Code  
$out_buchungen = $buchungen->render("buchungen");
?> 