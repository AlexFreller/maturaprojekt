<?php

include_once("config.php");

if (mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS) == false)
{
    echo "Fehler bei Server-Verbindung";
}
else if (mysql_select_db(PHPGRID_DBNAME) == false)
{
    echo "Fehler bei DB-Auswahl";
}
else
{

    $sql = "select distinct YEAR(datum) as jahr from buchungen order by jahr desc";
    $erg = mysql_query($sql);

    $jahre = array();

    for ($i = 0; $i < mysql_num_rows($erg); $i++)
    {
        $zeile = mysql_fetch_array($erg);
        $jahre[] = $zeile['jahr'];
    }
    mysql_free_result($erg);

    if (!in_array(date('Y'), $jahre))
    {
        $jahre[] = date('Y');
    }

    $buchungsjahre = "";
    
    foreach ($jahre as $jahr)
    {
        $buchungsjahre .="<li><a class='dropdown_buchungsjahre' href='#'>" . $jahr . "</a></li>";
    }
    
    if (!isset($_SESSION["jahr"]))
    {
        $_SESSION["jahr"] = date('Y');
    }
}

