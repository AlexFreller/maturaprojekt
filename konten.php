<?php
include 'login_check.php';
include 'lade_buchungsjahre.php';
include 'konten_grid.php';
include 'buchungen_detail.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <script src="Scripts/jquery-2.1.3.js"></script>
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="bootstrap/css/bootstrap-theme.css" rel="stylesheet" />
        <script src="bootstrap/js/bootstrap.js"></script>

        <link rel="stylesheet" type="text/css" media="screen" href="lib/js/themes/redmond/jquery-ui.custom.css"></link>     
        <link rel="stylesheet" type="text/css" media="screen" href="lib/js/jqgrid/css/ui.jqgrid.css"></link>     

        <style>
            a, label, span{
                font-size: 150%
            }
            tr.ui-search-toolbar select, input{
                height: 25px !important
            }
            /* increase font & row height */
            .ui-jqgrid *, .ui-widget, .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button { font-size:14px; }
            .ui-jqgrid tr.jqgrow td { height:30px;
                                    font-size: 150%}
                
            /* big toolbar icons */
            .ui-jqgrid .ui-jqgrid-pager .ui-pg-div span.ui-icon { margin: 4px; }
            .ui-jqgrid .ui-jqgrid-pager { height: 32px; }
            .ui-jqgrid .ui-jqgrid-pager .ui-pg-div { line-height: 35px; }

        </style>

        <script src="lib/js/jquery.min.js" type="text/javascript"></script> 
        <script src="lib/js/jqgrid/js/i18n/grid.locale-de.js" type="text/javascript"></script> 
        <script src="lib/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>     
        <script src="lib/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script> 

        <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Kassenprogramm</title>
    </head>
    <body>
        <?php
        include 'navbar.php';
        echo $out_konten;
        ?>
        <br/>
        <?php
        echo $out_buchungen_detail;
        ?>
    </body>
</html>