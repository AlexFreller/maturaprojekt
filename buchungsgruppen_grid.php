<?php
// include db config 
include_once("config.php");

include("lade_spaltenbreiten.php");

// set up DB
mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS);
mysql_select_db(PHPGRID_DBNAME);

// include and create object 
include(PHPGRID_LIBPATH . "inc/jqgrid_dist.php");
$buchungsgruppen = new jqgrid();

// set few params 
$optionen["caption"] = "Buchungsgruppen";
$optionen["autowidth"] = true; // expand grid to screen width 
$optionen["multiselect"] = false;
$optionen["height"] = "300";
$optionen["forceFit"] = true;

$optionen["add_options"] = array('width'=>'370');

$optionen["export"] = array("filename" => "Buchungsgruppen", "heading" => "Buchungsgruppen", "orientation" => "portrait", "paper" => "a4");
// for excel, sheet header 
$optionen["export"]["sheetname"] = "Buchungsgruppen";

// export filtered data or all data 
$optionen["export"]["range"] = "filtered"; // or "all" 

$optionen["detail_grid_id"] = "buchungen";

$buchungsgruppen->set_options($optionen);

$buchungsgruppen->set_actions(array(
    "add" => true, // allow/disallow add 
    "edit" => true, // allow/disallow edit 
    "delete" => true, // allow/disallow delete 
    "rowactions" => true, // show/hide row wise edit/del/save option 
    "export_excel" => true, // export excel button 
    "export_pdf" => true, // export pdf button 
    "autofilter" => true, // show/hide autofilter for search 
    "search" => "advance" // show single/multi field search condition (e.g. simple or advance)
        )
);

$buchungsgruppen->table = "buchungsgruppen";

$buchungsgruppen->select_command = "SELECT buchungsgruppen.ID, Name, Beschreibung,
                    sum(if(betrag > 0,betrag,0)) as Einnahmen,
                    sum(if(betrag < 0,betrag,0)) as Ausgaben,
                    SUM(ifnull(Betrag,0)) as Summe 
                    FROM buchungen RIGHT JOIN buchungsgruppen ON buchungen.BGruppe_ID = buchungsgruppen.ID
                    WHERE YEAR(datum) = ". $_SESSION["jahr"] . 
                    " GROUP BY buchungsgruppen.ID, Name, Beschreibung";
                    

// <editor-fold defaultstate="collapsed" desc="Spaltenformatierung">

$spalte = array();
$spalte["title"] = "ID";
$spalte["name"] = "ID";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["ID"][$_SESSION["benutzername"]];
$spalte["editable"] = false;
$spalte["hidden"] = true;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Name";
$spalte["name"] = "Name";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["Name"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalte["editrules"] = array("required" => true);
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Beschreibung";
$spalte["name"] = "Beschreibung";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["Beschreibung"][$_SESSION["benutzername"]];
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Einnahmen";
$spalte["name"] = "Einnahmen";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["Einnahmen"][$_SESSION["benutzername"]];
$spalte["editable"] = false;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Ausgaben";
$spalte["name"] = "Ausgaben";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["Ausgaben"][$_SESSION["benutzername"]];
$spalte["editable"] = false;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Summe";
$spalte["name"] = "Summe";
$spalte["width"] = $spaltenbreiten["buchungsgruppen"]["Summe"][$_SESSION["benutzername"]];
$spalte["editable"] = false;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$buchungsgruppen->set_columns($spalten, true);

//</editor-fold>
// <editor-fold defaultstate="collapsed" desc="Zeilenformatierung">

$zeile = array();
$zeile["column"] = "Summe";
$zeile["op"] = ">";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'green'";
$zeilen[] = $zeile;

$zeile = array();
$zeile["column"] = "Summe";
$zeile["op"] = "<";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'red'";
$zeilen[] = $zeile;

$zeile = array();
$zeile["column"] = "Einnahmen";
$zeile["op"] = ">";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'green'";
$zeilen[] = $zeile;

$zeile = array();
$zeile["column"] = "Einnahmen";
$zeile["op"] = "<";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'red'";
$zeilen[] = $zeile;

$zeile = array();
$zeile["column"] = "Ausgaben";
$zeile["op"] = ">";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'green'";
$zeilen[] = $zeile;

$zeile = array();
$zeile["column"] = "Ausgaben";
$zeile["op"] = "<";
$zeile["value"] = "0";
$zeile["cellcss"] = "'color':'red'";
$zeilen[] = $zeile;

$buchungsgruppen->set_conditional_css($zeilen);
//</editor-fold>
// render grid 
$out_buchungsgruppen = $buchungsgruppen->render("buchungsgruppen");
?>