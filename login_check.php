<?php
session_start();
if(!isset($_SESSION["ablaufzeit"]) || $_SESSION["ablaufzeit"] < time())
{   
    header('Location: index.php');
    exit();
}