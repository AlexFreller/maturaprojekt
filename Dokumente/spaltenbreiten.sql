-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 24. Dez 2015 um 14:16
-- Server-Version: 5.6.25
-- PHP-Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mpdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spaltenbreiten`
--

CREATE TABLE IF NOT EXISTS `spaltenbreiten` (
  `ID` int(11) NOT NULL,
  `Grid` varchar(20) NOT NULL,
  `Spaltenname` varchar(20) NOT NULL,
  `Breite` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `spaltenbreiten`
--

INSERT INTO `spaltenbreiten` (`ID`, `Grid`, `Spaltenname`, `Breite`) VALUES
(1, 'buchungen', 'ID', 20),
(2, 'buchungen', 'Datum', 482),
(3, 'buchungen', 'Betrag', 74),
(4, 'buchungen', 'Verwendungszweck', 616),
(5, 'buchungen', 'Empfaenger', 223),
(6, 'buchungen', 'Art', 275),
(7, 'buchungen', 'Konto_ID', 369),
(8, 'buchungen', 'BGruppe_ID', 369),
(23, 'buchungsgruppen', 'ID', 68),
(24, 'buchungsgruppen', 'Name', 492),
(25, 'buchungsgruppen', 'Beschreibung', 432),
(26, 'buchungsgruppen', 'Einnahmen', 266),
(27, 'buchungsgruppen', 'Ausgaben', 266),
(28, 'buchungsgruppen', 'Summe', 266),
(29, 'konten', 'ID', 307),
(30, 'konten', 'IBAN', 605),
(31, 'konten', 'Beschreibung', 456),
(32, 'konten', 'Saldo', 457);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `spaltenbreiten`
--
ALTER TABLE `spaltenbreiten`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `spaltenbreiten`
--
ALTER TABLE `spaltenbreiten`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
