<?php

include_once("config.php");

if (mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS) == false)
{
    echo "Fehler bei Server-Verbindung";
}
else if (mysql_select_db(PHPGRID_DBNAME) == false)
{
    echo "Fehler bei DB-Auswahl";
}
else
{
    $sql = "select Art,BGruppe_ID,Konto_ID from buchungen order by ID desc limit 1";
    $erg = mysql_query($sql);
    if ($erg == 0)
    {
        echo "Fehler beim laden der Spaltenbreiten";
    }
    else
    {
        for ($i = 0; $i < mysql_num_rows($erg); $i++)
        {
            $zeile = mysql_fetch_array($erg);
            $defaults["Art"] = $zeile["Art"];
            $defaults["BGruppe_ID"] = $zeile["BGruppe_ID"];
            $defaults["Konto_ID"] = $zeile["Konto_ID"];
        }
        mysql_free_result($erg);
        echo json_encode($defaults);
    }
}