-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 11. Nov 2015 um 14:26
-- Server-Version: 5.6.25
-- PHP-Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `mpdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `benutzer`
--

CREATE TABLE IF NOT EXISTS `benutzer` (
  `ID` int(11) NOT NULL,
  `Benutzername` varchar(50) NOT NULL,
  `Passwort` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `benutzer`
--

INSERT INTO `benutzer` (`ID`, `Benutzername`, `Passwort`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buchungen`
--

CREATE TABLE IF NOT EXISTS `buchungen` (
  `ID` int(10) NOT NULL,
  `Datum` date NOT NULL,
  `Betrag` decimal(10,2) NOT NULL,
  `Verwendungszweck` varchar(200) DEFAULT NULL,
  `Empfaenger` varchar(200) DEFAULT NULL,
  `Art` varchar(50) DEFAULT NULL,
  `Konto_ID` int(11) DEFAULT NULL,
  `BGruppe_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `buchungen`
--

INSERT INTO `buchungen` (`ID`, `Datum`, `Betrag`, `Verwendungszweck`, `Empfaenger`, `Art`, `Konto_ID`, `BGruppe_ID`) VALUES
(3, '2015-09-01', '123.00', 'asdf', 'Empfaenger1', 'Einnahme', 1, 2),
(4, '2014-12-01', '100.00', 'TEST', 'Empfaenger2', 'Einnahme', 1, 2),
(5, '2015-10-15', '100.00', 'asdf', 'Empfaenger21', 'Ausgabe', 1, 2),
(6, '2015-10-15', '100.00', 'asdf', 'empf', 'Ausgabe', 1, 2),
(7, '2015-10-09', '123.00', 'adsf', 'Empfaenger1', 'Ausgabe', 1, 2),
(10, '2015-10-01', '1234.00', 'adsf', 'asdf', 'Ausgabe', 1, 2),
(11, '2015-10-08', '123.00', 'adf', 'Empfaenger1', 'Ausgabe', 1, 2),
(12, '2015-10-16', '1234.00', 'asdf', 'e', 'Ausgabe', 1, 2),
(13, '2015-10-16', '-123.00', 'event', NULL, 'Ausgabe', 1, 2),
(14, '2015-10-16', '123.00', 'event', NULL, 'Einnahme', 1, 2),
(15, '2015-10-16', '1234.00', 'asdf', '', 'Einnahme', 1, 2),
(20, '2015-10-22', '-1234.00', 'TEST TRANSACTIONasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '', 'Einnahme', 1, 2),
(27, '2015-10-24', '1234.00', '122134', 'Empfaenger21', 'Einnahme', 1, 3),
(30, '2015-10-22', '-456.00', 'asdf', 'Empfaenger1', 'Ausgabe', 2, 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `buchungsgruppen`
--

CREATE TABLE IF NOT EXISTS `buchungsgruppen` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Beschreibung` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `buchungsgruppen`
--

INSERT INTO `buchungsgruppen` (`ID`, `Name`, `Beschreibung`) VALUES
(2, 'Buchungsgruppe2', 'Test'),
(3, 'Buchungsgruppe 1asdfsadf', 'Test'),
(4, 'asdf', 'asdf');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `konten`
--

CREATE TABLE IF NOT EXISTS `konten` (
  `ID` int(11) NOT NULL,
  `IBAN` varchar(50) NOT NULL,
  `Beschreibung` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `konten`
--

INSERT INTO `konten` (`ID`, `IBAN`, `Beschreibung`) VALUES
(1, 'ATxxxxxxxxxx123', 'test'),
(2, 'AT24xxxxxxxx289', 'Konto1'),
(3, 'asdf', 'asfd');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `benutzername_index` (`Benutzername`);

--
-- Indizes für die Tabelle `buchungen`
--
ALTER TABLE `buchungen`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Konto_ID` (`Konto_ID`),
  ADD KEY `BGruppe_ID` (`BGruppe_ID`);

--
-- Indizes für die Tabelle `buchungsgruppen`
--
ALTER TABLE `buchungsgruppen`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `konten`
--
ALTER TABLE `konten`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `benutzer`
--
ALTER TABLE `benutzer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT für Tabelle `buchungen`
--
ALTER TABLE `buchungen`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT für Tabelle `buchungsgruppen`
--
ALTER TABLE `buchungsgruppen`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `konten`
--
ALTER TABLE `konten`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `buchungen`
--
ALTER TABLE `buchungen`
  ADD CONSTRAINT `buchungen_ibfk_2` FOREIGN KEY (`BGruppe_ID`) REFERENCES `buchungsgruppen` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `buchungen_konten` FOREIGN KEY (`Konto_ID`) REFERENCES `konten` (`ID`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
