<?php
include_once("config.php");
include 'lade_spaltenbreiten.php';
mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS);
mysql_select_db(PHPGRID_DBNAME);


include(PHPGRID_LIBPATH . "inc/jqgrid_dist.php");
$benutzer = new jqgrid();


$optionen["caption"] = "Benutzer";
$optionen["autowidth"] = true; 
$optionen["multiselect"] = false;
$optionen["forceFit"] = true;
$optionen["height"] = "";
$optionen["reloadedit"] = true;

$benutzer->set_options($optionen);

$benutzer->set_actions(array(
    "add" => true,
    "edit" => true, 
    "delete" => true, 
    "rowactions" => true, 
    "export_excel" => false, 
    "export_pdf" => false,
    "autofilter" => true, 
    "search" => "simple" 
        )
);

$benutzer->table = "benutzer";

$benutzer->select_command = "SELECT * from benutzer";

$spalte = array();
$spalte["title"] = "ID";
$spalte["name"] = "ID";
$spalte["editable"] = false;
$spalte["hidden"] = true;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Benutzer";
$spalte["name"] = "Benutzer";
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$spalte = array();
$spalte["title"] = "Passwort";
$spalte["name"] = "Passwort";
$spalte["editable"] = true;
$spalte["hidden"] = false;
$spalten[] = $spalte;

$benutzer->set_columns($spalten, true);


function add_row($data) 
{      
        $data["params"]["Passwort"] = sha1($data["params"]["Passwort"]);
}

$e["on_insert"] = array("add_row", null, true); 
$e["on_update"] = array("add_row", null, true);
$benutzer->set_events($e);  

$out_benutzer = $benutzer->render("Benutzer");
?>