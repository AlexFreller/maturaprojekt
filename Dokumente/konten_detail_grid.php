<?php
$buchungen_detail = new jqgrid();

$grid["caption"] = "Buchungen";
$grid["sortname"] = 'Datum';
$grid["sortorder"] = "desc";
$grid["autowidth"] = true; // expand grid to screen width 
$grid["multiselect"] = false;


$buchungen_detail->set_options($grid);
$id = intval($_GET["rowid"]);

$buchungen_detail->table = "buchungen";

$buchungen_detail->select_command = "SELECT `buchungen`.`ID`, Datum, Betrag,
                    Verwendungszweck, Art,
                    konten.IBAN AS Konto, buchungsgruppen.Name as Buchungsgruppe
                    FROM buchungen INNER JOIN buchungsgruppen ON buchungen.BGruppe_ID = buchungsgruppen.ID
                                     INNER JOIN konten ON buchungen.Konto_ID = konten.ID
                                     WHERE buchungen.BGruppe_ID = $id AND buchungen.Datum = ";


// <editor-fold defaultstate="collapsed" desc="Spaltenforamtierung">
$col = array();
$col["title"] = "ID";
$col["name"] = "ID";
//$col["width"] = "50";
$col["editable"] = false;
$col["hidden"] = false;
$col["editrules"]["readonly"] = true;
$cols[] = $col;


$col = array();
$col["title"] = "Datum";
$col["name"] = "Datum";
//$col["width"] = "100";
$col["editable"] = true;
$col["hidden"] = false;
$col["formatter"] = "date";
$col["formatoptions"] = array("srcformat" => 'Y-m-d', "newformat" => 'd.m.Y');
$col["editrules"] = array("required" => true);
$cols[] = $col;

$col = array();
$col["title"] = "Betrag";
$col["name"] = "Betrag";
//$col["width"] = "150";
$col["editable"] = true;
$col["hidden"] = false;
$col["formatter"] = "number";
$col["formatoptions"] = array("thousandsSeparator" => ".",
    "decimalSeparator" => ",",
    "decimalPlaces" => 2);
$col["editrules"] = array("required" => true);
$cols[] = $col;




$buchungen_detail->set_columns($cols, true);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Zeilenformatierung">

$f = array();
$f["column"] = "Betrag";
$f["op"] = ">";
$f["value"] = "0";
$f["cellcss"] = "'color':'green'";
$f_conditions[] = $f;

$f = array();
$f["column"] = "Betrag";
$f["op"] = "<";
$f["value"] = "0";
$f["cellcss"] = "'color':'red'";
$f_conditions[] = $f;

$buchungen_detail->set_conditional_css($f_conditions);
//</editor-fold>

$buchungen_detail->set_actions(array(
    "add" => true, // allow/disallow add 
    "edit" => true, // allow/disallow edit 
    "delete" => true, // allow/disallow delete 
    "rowactions" => true, // show/hide row wise edit/del/save option 
    "export_excel" => true, // export excel button 
    "export_pdf" => true, // export pdf button 
    "autofilter" => false, // show/hide autofilter for search 
    "search" => "advance" // show single/multi field search condition (e.g. simple or advance)
        )
);

$out_buchungen_detail = $buchungen_detail->render("list2");
?>