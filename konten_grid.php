<?php
include_once("config.php");
include 'lade_spaltenbreiten.php';
mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS);
mysql_select_db(PHPGRID_DBNAME);

// include and create object 
include(PHPGRID_LIBPATH . "inc/jqgrid_dist.php");
$konten = new jqgrid();

// set few params 
$grid["caption"] = "Konten";
$grid["autowidth"] = true; // expand grid to screen width 
$grid["multiselect"] = false;
$grid["forceFit"] = true;
$grid["height"] = "300";

$grid["add_options"] = array('width'=>'370');

$grid["export"] = array("filename" => "Konten", "heading" => "Konten", "orientation" => "portrait", "paper" => "a4");
$grid["export"]["sheetname"] = "Konten";
$grid["export"]["range"] = "filtered"; // or "all" 

$grid["detail_grid_id"] = "buchungen";
$foreignkey = "buchungen.Konto_ID";
$konten->set_options($grid);

$konten->set_actions(array(
    "add" => true, // allow/disallow add 
    "edit" => true, // allow/disallow edit 
    "delete" => true, // allow/disallow delete 
    "rowactions" => true, // show/hide row wise edit/del/save option 
    "export_excel" => true, // export excel button 
    "export_pdf" => true, // export pdf button 
    "autofilter" => true, // show/hide autofilter for search 
    "search" => "advance" // show single/multi field search condition (e.g. simple or advance)
        )
);

// set database table for CRUD operations 
$konten->table = "konten";

$konten->select_command = "SELECT konten.ID,IBAN,Beschreibung, SUM(ifnull(buchungen.betrag,0)) AS Saldo 
                            FROM buchungen RIGHT JOIN konten ON buchungen.Konto_ID = konten.ID 
                            WHERE YEAR(datum) = ". $_SESSION["jahr"] .
                            " GROUP BY konten.ID, IBAN,Beschreibung";

// <editor-fold defaultstate="collapsed" desc="Spaltenformatierung">

$col = array();
$col["title"] = "ID";
$col["name"] = "ID";
$col["width"] = $spaltenbreiten["konten"]["ID"][$_SESSION["benutzername"]];
$col["editable"] = false;
$col["hidden"] = true;
$cols[] = $col;

$col = array();
$col["title"] = "IBAN";
$col["name"] = "IBAN";
$col["width"] = $spaltenbreiten["konten"]["IBAN"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$cols[] = $col;

$col = array();
$col["title"] = "Beschreibung";
$col["name"] = "Beschreibung";
$col["width"] = $spaltenbreiten["konten"]["Beschreibung"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$cols[] = $col;

$col = array();
$col["title"] = "Saldo";
$col["name"] = "Saldo";
$col["width"] = $spaltenbreiten["konten"]["Saldo"][$_SESSION["benutzername"]];
$col["editable"] = false;
$col["hidden"] = false;
$cols[] = $col;

$konten->set_columns($cols, true);

//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Zeilenformatierung">
$f = array();
$f["column"] = "Saldo";
$f["op"] = ">";
$f["value"] = "0";
$f["cellcss"] = "'color':'green'";
$f_conditions[] = $f;

$f = array();
$f["column"] = "Saldo";
$f["op"] = "<";
$f["value"] = "0";
$f["cellcss"] = "'color':'red'";
$f_conditions[] = $f;

$konten->set_conditional_css($f_conditions);
//</editor-fold>

// render grid 
$out_konten = $konten->render("konten");
?>