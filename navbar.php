<script>
    $(document).ready(function () {
        $(".dropdown_buchungsjahre").click(function () {

            var text = $(this).text();
            $.ajax({
                type: "POST",
                url: "setze_buchungsjahr.php",
                data: {jahr: text},
                success: function (data) {
                    location.reload();
                }
            });
        });

        $("#speichere_spaltenbreiten").click(function () {

            if ($("#gview_buchungen").length > 0)
            {
                speichere_spaltenbreiten("buchungen");
            }

            if ($("#gview_buchungsgruppen").length > 0)
            {
                speichere_spaltenbreiten("buchungsgruppen");
            }

            if ($("#gview_konten").length > 0)
            {
                speichere_spaltenbreiten("konten");
            }

        });
    });

    function speichere_spaltenbreiten(name)
    {
        var json = '{"tabellenname" : "' + name + '","spalten" : [';
        var spaltenname = "";
        var breite = "";

        $("#" + name + "_ID").parent().find("th").each(function () {

            spaltenname = $(this).attr("id");
            spaltenname = spaltenname.substring(spaltenname.indexOf("_") + 1);

            if (spaltenname == "act")
            {
                json = json.slice(0, -1);
                return;
            }

            breite = $(this).width();
            json += '{"spaltenname" : "' + spaltenname + '" , "breite" : "' + breite + '"},';
        });

        json += "]}";

        $.ajax({
            type: "POST",
            url: "speichere_spaltenbreiten.php",
            data: {"json": json},
            success: function (data)
            {
                if (data != "Erfolg")
                {
                    $("#fehler").append(data);
                }
            }
        });
    }

</script>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header"> 
            <a class="navbar-brand" href="buchungen.php"><img src="img/logo.png" width="20" style="float: left"/>  FF Aigen - Kassenprogramm</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a href="buchungen.php">Buchungen</a></li>
                <li><a href="buchungsgruppen.php">Buchungsgruppen</a></li>
                <li><a href="konten.php">Konten</a></li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Buchungsjahre<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php echo $buchungsjahre; ?>
                    </ul>
                </li>
                <li><a href="#" id="speichere_spaltenbreiten">Spaltenbreiten speichern</a> </li>
                <li><a href="benutzer.php" id="benutzer">Benutzer</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="logout.php">Abmelden</a></li>
            </ul>
        </div>
    </div>
</nav>