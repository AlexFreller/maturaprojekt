<?php

include_once("config.php");

if (mysql_connect(PHPGRID_DBHOST, PHPGRID_DBUSER, PHPGRID_DBPASS) == false)
{
    echo "Fehler bei Server-Verbindung";
}
else if (mysql_select_db(PHPGRID_DBNAME) == false)
{
    echo "Fehler bei DB-Auswahl";
}
else
{
    $sql = "select Grid,Spaltenname,Breite,Benutzername from spaltenbreiten,benutzer where spaltenbreiten.Benutzer_ID = benutzer.ID";
    $erg = mysql_query($sql);
    if ($erg == 0)
    {
        echo "Fehler beim laden der Spaltenbreiten";
    }
    else
    {
        for ($i = 0; $i < mysql_num_rows($erg); $i++)
        {
            $zeile = mysql_fetch_array($erg);
            $spaltenbreiten[$zeile["Grid"]][$zeile["Spaltenname"]][$zeile["Benutzername"]] = $zeile["Breite"];
        }
        mysql_free_result($erg);
    }
}
