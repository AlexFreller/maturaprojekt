<?php

$host = "localhost";
$dbname = "mpdb";
$user = "root";
$passwd = "";

$link = mysqli_connect($host, $user, $passwd, $dbname);
if (!$link) {
    echo "Fehler bei Server-Verbindung";
} 
else {
    $datum = $_POST['datum'];
    $betrag = $_POST['betrag'];
    $verwendungszweck = $_POST['verwendungszweck'];
    $ziel = $_POST['ziel'];
    $quelle = $_POST['quelle'];
    $buchungsgruppe = $_POST['buchungsgruppe'];



    $query_ziel = "INSERT INTO buchungen (`ID`, `Datum`, `Betrag`, `Verwendungszweck`, `Art`, `Konto_ID`, `BGruppe_ID`) "
            . "VALUES (NULL,'$datum',$betrag,'$verwendungszweck', 'Umbuchung', $ziel, $buchungsgruppe);";
    $query_quelle = "INSERT INTO buchungen (`ID`, `Datum`, `Betrag`, `Verwendungszweck`, `Art`, `Konto_ID`, `BGruppe_ID`) "
            . "VALUES (NULL,'$datum'," . $betrag * (-1) . ", '$verwendungszweck', 'Umbuchung', $quelle, $buchungsgruppe);";
    
    if (!mysqli_query($link,"START TRANSACTION")) {
        echo "Fehler bei TRANSACTION";
    } 
    else if (!mysqli_query($link,$query_ziel)) {
        echo "Fehler bei einfügen der Ziel-Buchung";
    } 
    else if (!mysqli_query($link,$query_quelle)) {
        echo "Fehler bei einfügen der Quell-Buchung";
    } 
    else if (!mysqli_query($link,"COMMIT")) {
        echo "Fehler bei COMMIT";
    } 
    else {
        echo "Erfolg";
    }
}
?>