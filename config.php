<?php
// Verbindungsoptionen für die Datenbank
define("PHPGRID_DBTYPE","Mysql");       //Datenbanktyp 
define("PHPGRID_DBHOST","localhost");   //Datenbankhost
define("PHPGRID_DBUSER","root");        //Benutzername
define("PHPGRID_DBPASS","root");        //Passwort
define("PHPGRID_DBNAME","mpdb");        //Datenbankname

// Verbinde automatisch mit Datenbank
define("PHPGRID_AUTOCONNECT",0);

// Stammpfad der Bibliotheken
define("PHPGRID_LIBPATH",dirname(__FILE__).DIRECTORY_SEPARATOR."lib".DIRECTORY_SEPARATOR);
