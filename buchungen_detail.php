<?php

$buchungen_detail = new jqgrid();

$option["caption"] = "Buchungen";
$option["sortname"] = 'Datum';
$option["sortorder"] = "desc";
$option["autowidth"] = true;


$option["export"] = array("filename" => "Buchungen", "heading" => "Buchungen", "orientation" => "landscape", "paper" => "a4");
$option["export"]["sheetname"] = "Buchungen";
$option["export"]["range"] = "filtered";

$option["add_options"] = array('width' => '370');
$option["edit_options"] = array('width' => '370');

$buchungen_detail->set_options($option);

$buchungen_detail->set_actions(array(
    "add" => true, // allow/disallow add 
    "edit" => true, // allow/disallow edit 
    "delete" => true, // allow/disallow delete 
    "rowactions" => true, // show/hide row wise edit/del/save option 
    "export_excel" => true, // export excel button 
    "export_pdf" => true, // export pdf button 
    "autofilter" => false, // show/hide autofilter for search 
    "search" => "advance" // show single/multi field search condition (e.g. simple or advance)
        )
);

$id = intval($_GET["rowid"]);


$buchungen_detail->table = "buchungen";

if(strpos($_SERVER['REQUEST_URI'], 'buchungsgruppen') !== false)
{
    //echo "buchungsgruppen";
    $buchungen_detail->select_command = "SELECT `buchungen`.`ID`, Datum, Betrag, Verwendungszweck, Empfaenger, Art,
                    konten.IBAN AS Konto_ID, buchungsgruppen.Name as BGruppe_ID
                    FROM buchungen INNER JOIN buchungsgruppen ON buchungen.BGruppe_ID = buchungsgruppen.ID
                                   INNER JOIN konten ON buchungen.Konto_ID = konten.ID
                                   WHERE BGruppe_ID = $id AND YEAR(buchungen.datum) = " . $_SESSION["jahr"];
}
if(strpos($_SERVER['REQUEST_URI'], 'konten') !== false)
{
    //echo "konten";
    $buchungen_detail->select_command = "SELECT `buchungen`.`ID`, Datum, Betrag, Verwendungszweck, Empfaenger, Art,
                    konten.IBAN AS Konto_ID, buchungsgruppen.Name as BGruppe_ID
                    FROM buchungen INNER JOIN buchungsgruppen ON buchungen.BGruppe_ID = buchungsgruppen.ID
                                   INNER JOIN konten ON buchungen.Konto_ID = konten.ID
                                   WHERE Konto_ID = $id AND YEAR(buchungen.datum) = " . $_SESSION["jahr"];
}




// <editor-fold defaultstate="collapsed" desc="Spaltenforamtierung">
$col = array();
$col["title"] = "ID";
$col["name"] = "ID";
$col["width"] = $spaltenbreiten["buchungen"]["ID"][$_SESSION["benutzername"]];
$col["editable"] = false;
$col["hidden"] = true;
$col["editrules"]["readonly"] = true;
$col["export"] = false;
$cols[] = $col;


$col = array();
$col["title"] = "Datum";
$col["name"] = "Datum";
$col["width"] = $spaltenbreiten["buchungen"]["Datum"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["formatter"] = "date";
$col["formatoptions"] = array("srcformat" => 'Y-m-d', "newformat" => 'd.m.Y');
$col["editrules"] = array("required" => true);
$cols[] = $col;

$col = array();
$col["title"] = "Betrag";
$col["name"] = "Betrag";
$col["width"] = $spaltenbreiten["buchungen"]["Betrag"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["formatter"] = "number";
$col["formatoptions"] = array("thousandsSeparator" => ".",
    "decimalSeparator" => ",",
    "decimalPlaces" => 2);
$col["editrules"] = array("required" => true);
$cols[] = $col;

$col = array();
$col["title"] = "Verwendungszweck";
$col["name"] = "Verwendungszweck";
$col["width"] = $spaltenbreiten["buchungen"]["Verwendungszweck"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["edittype"] = "textarea";
$cols[] = $col;

$col = array();
$col["title"] = "Empfaenger";
$col["name"] = "Empfaenger";
$col["width"] = $spaltenbreiten["buchungen"]["Empfaenger"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["formatter"] = "autocomplete";
$col["formatoptions"] = array("sql" => "SELECT distinct Empfaenger as k, Empfaenger as v from buchungen",
    "search_on" => "Empfaenger",
    "update_field" => "Empfaenger");
$cols[] = $col;

$col = array();
$col["title"] = "Art";
$col["name"] = "Art";
$col["width"] = $spaltenbreiten["buchungen"]["Art"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["edittype"] = "select";
$col["editoptions"] = array("value" => 'E:E;A:A;K:K');
$cols[] = $col;

$col = array();
$col["title"] = "Konto";
$col["name"] = "Konto_ID";
//$col["dbname"] = "buchungen.Konto_ID";
$col["width"] = $spaltenbreiten["buchungen"]["Konto_ID"][$_SESSION["benutzername"]];
$col["align"] = "left";
$col["search"] = true;
$col["editable"] = true;
$col["edittype"] = "select";
$values_konto = $buchungen_detail->get_dropdown_values("select distinct ID as k, IBAN as v from konten");
$col["editoptions"] = array("value" => $values_konto);
$cols[] = $col;

$col = array();
$col["title"] = "Buchungsgruppe";
$col["name"] = "BGruppe_ID";
//$col["dbname"] = "buchungen.BGruppe_ID";
$col["width"] = $spaltenbreiten["buchungen"]["BGruppe_ID"][$_SESSION["benutzername"]];
$col["editable"] = true;
$col["hidden"] = false;
$col["edittype"] = "select";
$values_BG = $buchungen_detail->get_dropdown_values("select ID as k,Name as v from buchungsgruppen");
$col["editoptions"] = array("value" => $values_BG);
$cols[] = $col;



$buchungen_detail->set_columns($cols, true);

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Zeilenformatierung">

$f = array();
$f["column"] = "Betrag";
$f["op"] = ">";
$f["value"] = "0";
$f["cellcss"] = "'color':'green'";
$f_conditions[] = $f;

$f = array();
$f["column"] = "Betrag";
$f["op"] = "<";
$f["value"] = "0";
$f["cellcss"] = "'color':'red'";
$f_conditions[] = $f;

$buchungen_detail->set_conditional_css($f_conditions);
//</editor-fold>



$e["on_insert"] = array("add_client", null, true);
$e["on_update"] = array("add_client", null, true);
$buchungen_detail->set_events($e);

function add_client($data)
{
    if ($data["params"]["Art"] == "Ausgabe" && $data["params"]["Betrag"] > 0)
    {

        $data["params"]["Betrag"] = $data["params"]["Betrag"] * (-1);
    }
    else if ($data["params"]["Art"] == "Einnahme" && $data["params"]["Betrag"] < 0)
    {

        $data["params"]["Betrag"] = $data["params"]["Betrag"] * (-1);
    }
}

$out_buchungen_detail = $buchungen_detail->render("buchungen");
?>